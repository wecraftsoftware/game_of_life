package com.makingdevs

import spock.lang.*

class GameOfLifeTest extends Specification {

    @Unroll
    def "Organism exist with size #size"() {
        given:
            Organism organism = new Organism(size)
        when:
            def cells = organism.cells
        then:
            organism
            cells.size() == size
            cells.every { it.size() == size }
        where:
            size << (3 .. 10)
    }

    @Unroll
    def "Populate the #size x #size organism in position (#x,#y)"() {
        given:
            Organism organism = new Organism(size)
        when:
            organism.populate(x, y)
        then:
            organism.cells[x][y] == 1
        where:
            size | x | y
            3    | 1 | 1
            3    | 1 | 2
            4    | 2 | 2
            4    | 0 | 0
            5    | 4 | 4
    }

    def "Getting neighbors in (#x, #y)"() {
        given:
            Organism organism = new Organism(3)
        and:
            organism.populate(1,1)
            organism.populate(0,2)
        when:
            def neighbors = organism.getNeighbors(x,y)
        then:
            neighbors == result
        where:
            x | y || result
            0 | 0 || [0,1,0,0,0,0,0,1]
            0 | 2 || [0,0,1,0,0,0,0,0]
            2 | 0 || [0,0,1,0,0,1,0,0]
            2 | 2 || [1,0,0,0,1,0,0,0]

/*          x | y || result
            0 | 0 || [0,1,0,1,0,0,0,0]
            0 | 2 || [0,0,1,0,1,0,0,0]
            2 | 0 || [0,0,0,1,0,0,0,0]
            2 | 2 || [0,0,1,0,1,0,0,0] */
    }

    @Unroll("Given an organism, when we get the neighbors at #x, #y, then neighbors are #result ")
    def "Getting neighbors in (#x, #y) not squared"() {
        given:
        Organism organism = new Organism(3,4)
        and:
        organism.populate(1,1)
        organism.populate(0,2)
        when:
        def neighbors = organism.getNeighbors(x,y)
        then:
        neighbors == result
        where:
        x | y || result
        0 | 0 || [0,0,0,0,0,0,0,1]
        0 | 2 || [0,0,1,0,0,0,0,0]
        2 | 0 || [0,0,0,0,0,1,0,0]
        2 | 2 || [1,0,0,0,1,0,0,0]
    }

    def """Any live cell with fewer than two live neighbours dies, \
           as if caused by underpopulation."""(){
      given:
        Organism organism = new Organism(3)
      and:
        organism.populate(1,1)
        organism.populate(0,2)
      and:
        def oldState = organism.cells[1][1]
      when:
        def nextState = organism.rulesFor(1,1)
      then:
        nextState != oldState
        nextState == 0
    }



        def """Any live cell with two  live neighbours lives on to the next generation.."""(){
      given:
        Organism organism = new Organism(3)
      and:
        organism.populate(1,1)
        organism.populate(0,2)
        organism.populate(1,0)

      and:
        def oldState = organism.cells[1][1]
      when:
        def nextState = organism.rulesFor(1,1)
      then:
        nextState == oldState
        nextState == 1
    }


    def """Any live cell with more than three live neighbours dies, as if by overpopulation"""(){
      given:
        Organism organism = new Organism(3)
      and:
        organism.populate(0,0)
        organism.populate(1,1)
        organism.populate(0,2)
        organism.populate(1,0)
        organism.populate(2,0)
      and:
        def oldState = organism.cells[1][1]
      when:
        def nextState = organism.rulesFor(1,1)
      then:
        nextState != oldState
        nextState == 0
    }

    def "Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction"(){
        given:
            Organism organism = new Organism(3)
        and:
          organism.populate(0,0)
          organism.populate(0,2)
          organism.populate(2,0)
        and:
          def oldState = organism.cells[1][1]
        when:
          def nextState = organism.rulesFor(1,1)
        then:
          nextState != oldState
          nextState == 1
    }

    def """Evolve an organism"""(){
      given:
      Organism organism = new Organism(3)
      and:
      def someCells = [
        [1,0,1],
        [0,0,0],
        [1,0,0]
      ]
      organism.cells = someCells
      when:
      organism.evolve()
      then:
      organism.cells == [[1,1,1],[1,1,1],[1,1,1]]
    }

}
