package com.makingdevs

class Organism {

    def cells

    Organism(int n) { cells = new int[n][n] }
    Organism(int x, int y) { cells = new int[x][y] }

    void populate(int x, int y) {
        if (x >= cells.size()) { throw new RuntimeException("X-axis out of bounds") }
        if (y >= cells[0].size()) { throw new RuntimeException("Y-axis out of bounds") }
        cells[x][y] = 1
    }

    int[] getNeighbors(int x, int y){
        int max_x = cells.size() -1
        int max_y = cells[x].size() -1

        int x_p1 = getPosition(x, max_x, +1)
        int x_m1 = getPosition(x, max_x, -1)
        int y_p1 = getPosition(y, max_y, +1)
        int y_m1 = getPosition(y, max_y, -1)

        [ cells[x_m1][y_m1], cells[x][y_m1], cells[x_p1][y_m1],
          cells[x_m1][y],                    cells[x_p1][y],
          cells[x_m1][y_p1], cells[x][y_p1], cells[x_p1][y_p1]]
    }

    int getPosition(int position, int max, int sum){
        if(position + sum > max){
            return 0
        } else if(position + sum < 0){
            return max
        }
        position + sum
    }

    int rulesFor(int x, int y){
      def neighbors = getNeighbors(x,y)
      def cell = cells[x][y]
      def cantVecinosVivos = neighbors.sum()
      if (cell == 1) {
          return cantVecinosVivos < 2 || cantVecinosVivos > 3 ? 0 : 1
      } else {
        return cantVecinosVivos == 3 ? 1 : 0
      }
    }

    void evolve(){
        def cellEnvolve = new int[cells.size()][cells[0].size()]
      for(def i = 0; i < cells.size(); i++){
          for(def j = 0; j < cells.size(); j++){
              cellEnvolve[i][j] = rulesFor(i,j)
          }
      }
        cells = cellEnvolve
    }

    String getStructure(){
      String t = ""
      cells.each{ r ->
        t += r.join('').replace('0', "@|red ·|@").replace('1', "@|green *|@")
        t+="\n"
      }
      t
    }

}
